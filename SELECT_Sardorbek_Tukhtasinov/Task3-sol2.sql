WITH last_acting AS (
    SELECT 
        actor_id, 
        MAX(last_update) as last_acting_time 
    FROM 
        Film_Actor 
    GROUP BY 
        actor_id
)
SELECT 
    a.actor_id, 
    a.first_name, 
    a.last_name, 
    la.last_acting_time 
FROM 
    Actor a 
JOIN 
    last_acting la ON a.actor_id = la.actor_id
ORDER BY 
    last_acting_time ASC;
