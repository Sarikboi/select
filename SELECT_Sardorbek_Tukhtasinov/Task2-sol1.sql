SELECT
    f.title,
    f.rating,
    COUNT(r.rental_id) AS total_rentals,
    CASE 
        WHEN f.rating = 'G' THEN 'All ages'
        WHEN f.rating = 'PG' THEN 'Not suitable for children'
        WHEN f.rating = 'PG-13' THEN 'Inappropriate for children under 13'
        WHEN f.rating = 'R' THEN 'Under 17 requires adult'
        WHEN f.rating = 'NC-17' THEN 'No one 17 and under admitted'
        ELSE 'No Rating Information'
    END as expected_audience_age
FROM 
    Film f
JOIN 
    Inventory i ON f.film_id = i.film_id
JOIN 
    Rental r ON i.inventory_id = r.inventory_id
GROUP BY
    f.film_id
ORDER BY
    total_rentals DESC
LIMIT 5;
