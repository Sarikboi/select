WITH rental_counts AS (
    SELECT
        i.film_id,
        COUNT(r.rental_id) AS total_rentals
    FROM 
        Inventory i
    JOIN 
        Rental r ON i.inventory_id = r.inventory_id
    GROUP BY
        i.film_id
)

SELECT
    f.title,
    f.rating,
    rc.total_rentals,
    CASE 
        WHEN f.rating = 'G' THEN 'All ages'
        WHEN f.rating = 'PG' THEN 'Not suitable for children'
        WHEN f.rating = 'PG-13' THEN 'Inappropriate for children under 13'
        WHEN f.rating = 'R' THEN 'Under 17 requires adult'
        WHEN f.rating = 'NC-17' THEN 'No one 17 and under admitted'
        ELSE 'No Rating Information'
    END as expected_audience_age
FROM 
    Film f
JOIN
    rental_counts rc ON f.film_id = rc.film_id
ORDER BY
    rc.total_rentals DESC
LIMIT 5;

