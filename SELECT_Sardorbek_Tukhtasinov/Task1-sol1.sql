WITH staff_revenue AS (
    SELECT 
        s.staff_id, 
        s.store_id,
        SUM(p.amount) AS total_revenue
    FROM 
        staff s 
    JOIN 
        rental r ON s.staff_id = r.staff_id
    JOIN 
        payment p ON r.rental_id = p.rental_id
    WHERE 
        EXTRACT(YEAR FROM p.payment_date) = 2017 
    GROUP BY 
        s.staff_id, 
        s.store_id
),

staff_rank AS (
    SELECT 
        s.staff_id,
        s.first_name,
        s.last_name,
        sr.store_id,
        sr.total_revenue,
        ROW_NUMBER() OVER (PARTITION BY sr.store_id ORDER BY sr.total_revenue DESC) AS revenue_rank
    FROM 
        staff s
    JOIN 
        staff_revenue sr ON s.staff_id = sr.staff_id
)

SELECT 
    staff_id,
    first_name,
    last_name,
    store_id,
    total_revenue
FROM 
    staff_rank
WHERE 
    revenue_rank = 1;
