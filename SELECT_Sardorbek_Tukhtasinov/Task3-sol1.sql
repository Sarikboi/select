SELECT 
    a.actor_id,
    a.first_name,
    a.last_name,
    MAX(fa.last_update) AS last_acted
FROM 
    Actor a
INNER JOIN 
    Film_Actor fa ON a.actor_id = fa.actor_id
GROUP BY 
    a.actor_id
ORDER BY 
    last_acted
